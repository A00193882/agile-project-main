package productCode;

import daoClasses.DBController;

public class DayOfWeek {
	
	private int dayId;
	private String dayName;
	DBController database= new DBController();
	
	
	public int getDayId() {
		return dayId;
	}
	public void setDayId(int dayId) {
		this.dayId = dayId;
	}
	public String getDayName() {
		return dayName;
	}
	public void setDayName(String dayName) {
		this.dayName = dayName;
	}
	
	
	public boolean selectday(int id,String day)throws MyExceptionHander{
		
		try{
			 database.initiate_db_conn();
			}catch(Exception e){
				e.printStackTrace();
			}
					
			 if(database.selectday(id, day)){
				return true;
			 }
			 
			 else  throw new MyExceptionHander("Invalid day");
		 }
	
	

	public boolean selectsunday(int id,String day)throws MyExceptionHander{
		
		try{
			 database.initiate_db_conn();
			}catch(Exception e){
				e.printStackTrace();
			}
					
			 if(database.selectsunday(id, day)){
				return true;
			 }
			 
			 else  throw new MyExceptionHander("Invalid day");
		 }
	
	public boolean selectmonday(int id,String day)throws MyExceptionHander{
		
		try{
			 database.initiate_db_conn();
			}catch(Exception e){
				e.printStackTrace();
			}
					
			 if(database.selectmonday(id, day)){
				return true;
			 }
			 
			 else  throw new MyExceptionHander("Invalid day");
		 }
	
	public boolean selecttuesday(int id,String day)throws MyExceptionHander{
		
		try{
			 database.initiate_db_conn();
			}catch(Exception e){
				e.printStackTrace();
			}
					
			 if(database.selecttuesday(id, day)){
				return true;
			 }
			 
			 else  throw new MyExceptionHander("Invalid day");
		 }
	
	public boolean selectwednesday(int id,String day)throws MyExceptionHander{
		
		try{
			 database.initiate_db_conn();
			}catch(Exception e){
				e.printStackTrace();
			}
					
			 if(database.selectwednesday(id, day)){
				return true;
			 }
			 
			 else  throw new MyExceptionHander("Invalid day");
		 }
	
	public boolean selectthursday(int id,String day)throws MyExceptionHander{
		
		try{
			 database.initiate_db_conn();
			}catch(Exception e){
				e.printStackTrace();
			}
					
			 if(database.selectthursday(id, day)){
				return true;
			 }
			 
			 else  throw new MyExceptionHander("Invalid day");
		 }
	
	public boolean selectfriday(int id,String day)throws MyExceptionHander{
		
		try{
			 database.initiate_db_conn();
			}catch(Exception e){
				e.printStackTrace();
			}
					
			 if(database.selectfriday(id, day)){
				return true;
			 }
			 
			 else  throw new MyExceptionHander("Invalid day");
		 }
	
	public boolean selectsaturday(int id,String day)throws MyExceptionHander{
		
		try{
			 database.initiate_db_conn();
			}catch(Exception e){
				e.printStackTrace();
			}
					
			 if(database.selectsaturday(id, day)){
				return true;
			 }
			 
			 else  throw new MyExceptionHander("Invalid day");
		 }
}
