package productCode;

import daoClasses.DBController;

public class Order {
	
	private int orderId;
	private String deliveryType;
	
	DBController database= new DBController();
	
	private int quantity;
	private Publication publicationId;
	private Customer customer;
	private int dayId;
	private int deliveryConfigurationId;
	private String paymentStatus;
	private String deliveryStatus;
	private String deliverySuccess = "Successful";
	private String deliveryUnsuccessful = "Unsuccessful";
	private DBController db;

	public Order() {
		this.orderId=orderId;
		this.deliveryType=deliveryType;
	}

	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getPublicationId() {
		return publicationId.getPublicationId();
	}

	public int getCustomerId() {
		return customer.getCustomerId();
	}

	public int getDayId() {
		return dayId;
	}
	public void setDayId(int dayId) {
		this.dayId = dayId;
	}
	public int getLocationId() {
		return getLocationId();
	}

	public int getDeliveryConfiguration() {
		return deliveryConfigurationId;
	}
	public void setDeliveryConfiguration(int deliveryConfiguration) {
		this.deliveryConfigurationId = deliveryConfiguration;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentType() {
		return deliveryType;
	}
	public void setPaymentType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	
	
	public boolean selectnewspaper(int id, String type)throws MyExceptionHander{
		
		try{
			 database.initiate_db_conn();
			}catch(Exception e){
				e.printStackTrace();
			}
			
			 if(database.selectnewspaper(id, type)){
				return true;
			 }
			 
		  else throw new MyExceptionHander("select newspaper Unsuccesssful");
		 }
	
	public boolean selectmagzine(int id, String type)throws MyExceptionHander{
		
		try{
			 database.initiate_db_conn();
			}catch(Exception e){
				e.printStackTrace();
			}
			
			 if(database.selectmagazine(id, type)){
				return true;
			 }
			 
		  else throw new MyExceptionHander("select magazin Unsuccesssful");
		 }	
}