package productCode;

import java.util.List;
import daoClasses.DBController;
import daoClasses.dbExceptionHander;


public class Publication {
	
	private int publicationId;
	private String name;
	private int cost; 
	
	DBController database= new DBController();
	private int pub_id;
	private String pubName;
	

	public Publication(int pub_id, String pubName, int cost) {
		super();
		this.pub_id = pub_id;
		this.pubName = pubName;
		this.cost = cost;
	}
	
	public int getPublicationId() {
		return publicationId;
	}
	public void setPublicationId(int publicationId) {
		this.publicationId = publicationId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	public int getPub_id() {
		return pub_id;
	}

	public void setPub_id(int pub_id) {
		this.pub_id = pub_id;
	}

	public String getPubName() {
		return pubName;
	}

	public void setPubName(String pubName) {
		this.pubName = pubName;
	}
	
}
