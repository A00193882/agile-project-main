package testCases;

import daoClasses.DBController;
import junit.framework.TestCase;
import productCode.MyExceptionHander;
import productCode.PublicationTools;

public class PublicationTest extends TestCase {
	
	
	// Test No: 001
	// Test Objective: Test list of Publication menu
	// Input(s): none
	// Expected Outputs: list Publication menu successfully (boolean true)

public void testPublicationlist() throws MyExceptionHander{

	 PublicationTools publicationTools=new PublicationTools();
	
	 assertSame(true, publicationTools.publicationList());
  }
}
