package testCases;

import junit.framework.TestCase;
import productCode.DayOfWeek;
import productCode.MyExceptionHander;


public class DayOfWeekTest extends TestCase {
	   //Text  NO.1 
	   //text Objective: select day of Sunday
	   //input(s): user id:1; day:Sunday
	   //excepted output:select day successful
	public void testselectdaySunday() throws MyExceptionHander{
		 DayOfWeek dayofweek=new DayOfWeek();
		assertSame(true,dayofweek.selectsunday(1,"Sunday"));
		}	
	
	    //Text  NO.2
	   //text Objective: select day of Monday
	   //input(s): user id:2; day:Monday
	   //excepted output:select day successful
	public void testselectdayMonday() throws MyExceptionHander{
		 DayOfWeek dayofweek=new DayOfWeek();
		assertSame(true,dayofweek.selectmonday(2,"Monday"));
		}	
	
	    //Text  NO.3
	   //text Objective: select day of Tuesday
	   //input(s): user id:3; day:Tuesday
	   //excepted output:select day successful
public void testselectdayTuesday() throws MyExceptionHander{
		 DayOfWeek dayofweek=new DayOfWeek();
			assertSame(true, dayofweek.selecttuesday(3,"Tuesday"));
		}	
	    //Text  NO.4
	   //text Objective: select day of Wednesday
	   //input(s): user id:4; day:Wednesday
	   //excepted output:select day successful
	public void testselectdayWednesday() throws MyExceptionHander{
		 DayOfWeek dayofweek=new DayOfWeek();		
			assertSame(true,dayofweek.selectwednesday(4,"Wednesday"));
		}	
	     //Text  NO.5
	   //text Objective: select day of Thursday
	   //input(s): user id:5; day:Thursday
	   //excepted output:select day successful
	public void testselectdayThursday() throws MyExceptionHander{
		 DayOfWeek dayofweek=new DayOfWeek();
			assertSame(true,dayofweek.selectthursday(5,"Thursday"));
		}	
	
	     //Text  NO.6
	   //text Objective: select day of Friday
	   //input(s): user id:6; day:Friday
	   //excepted output:select day successful
	public void testselectdayFriday() throws MyExceptionHander{
		 DayOfWeek dayofweek=new DayOfWeek();
			assertSame(true,dayofweek.selectfriday(6,"Friday"));
		}	
	    //Text  NO.7
	   //text Objective: select day of Saturday
	   //input(s): user id:7; day:Saturday
	   //excepted output:select day successful
	public void testselectdaySaturday() throws MyExceptionHander{
		 DayOfWeek dayofweek=new DayOfWeek();
			assertSame(true,dayofweek.selectsaturday(7,"Saturday"));
		}	
	    //Text  NO.8
	   //text Objective: select day of Saturday
	   //input(s): user id:8; day:Saturday
	   //excepted output:select day Unsuccessful
	public void testselectdayFaultday(){
		 DayOfWeek dayofweek=new DayOfWeek();
		 try {
			 dayofweek.selectday(8,"Saturday");
		} catch (MyExceptionHander e) {
			assertSame("Invalid day",e.getMessage());
		}	
	 }   
	
}
