package testCases;

import daoClasses.DBController;
import junit.framework.TestCase;
import productCode.Employee;
import productCode.MyExceptionHander;
import productCode.Order;
import productCode.OrderExceptionHander;

public class OrderTest extends TestCase {
	
	   //Text  NO.1 
	   //text Objective: select type of newspaper
	   //input(s): user id:1; type:newspaper
	   //excepted output:select successful
 public void testselectorderNewspaper() throws MyExceptionHander{
		 Order order=new Order();
		 
		 assertSame(true,order.selectnewspaper(1,"newspaper"));
	 } 
	 
	   //Text  NO.2
	   //text Objective: select type of magazine
	   //input(s): user id:1; type:magazine
	   //excepted output:select successful
 public void testselectorderMagazine() throws MyExceptionHander{
		 Order order=new Order();
		
		 assertSame(true,order.selectmagzine(1,"magazine"));
		}	
	 
	   //Text  NO.3
	   //text Objective: select type of cartoon
	   //input(s): user id:1; type:cartoon
	   //excepted output:select unsuccessful
 public void testselectorderCartoon(){
		 Order order=new Order();
		 try {
			 order.selectmagzine(5,"Cartoon");
		} catch (MyExceptionHander e) {
			assertSame("select magazin Unsuccesssful",e.getMessage());
		}	
    }    
}
