package testCases;

import junit.framework.TestCase;
import productCode.Employee;
import productCode.MyExceptionHander;

public class EmployeeTest extends TestCase {
	    //Text  NO.1
	   //text Objective: check both true username and password
	   //input(s): user name:Amy; password:A1234567
	   //excepted output:login successful
 public void testchecklogin1() throws MyExceptionHander{
	 Employee employee=new  Employee();

		assertSame(true,employee.checklogin("Amy1","A1234567"));
	}	

        //Text  NO.2
	   //text Objective: check true username and false password(the password length less than 7)
	   //input(s): user name:Amy; password:A12345
	   //excepted output:login faild
     
    public void testchecklogin2() {
    	 Employee employee=new  Employee();
		 try {
			 employee.checklogin("Amy1","A12345");
		} catch (MyExceptionHander e) {
			// TODO Auto-generated catch block
			assertSame("Invalid password",e.getMessage());
		}	
	 }
     
     
       //Text  NO.3
	   //text Objective: check false username and true password
	   //input(s): user name:Amy; password:A1234567
	   //excepted output:login faild
   
 public void testchecklogin3() {
  	 Employee employee=new  Employee();
		 try {
			 employee.checklogin("Amy","A1234567");
		} catch (MyExceptionHander e) {
			assertSame("Invalid username or password",e.getMessage());
		}	
	 }  
   
   //Text  NO.4
   //text Objective: the username and password both empty
   //input(s): user name:empty; password:empty
   //excepted output:login faild

public void testchecklogin4() {
	 Employee employee=new  Employee();
	 try {
		 employee.checklogin("","");
	} catch (MyExceptionHander e) {
		assertSame("Username and password can't be blank",e.getMessage());
	}	
 } 

     //Text  NO.5
     //text Objective: check false username and false password
     //input(s): user name:Amy; password:1234567
     //excepted output:login faild

public void testchecklogin5() {
	 Employee employee=new  Employee();
	 try {
		 employee.checklogin("Amy","A12345678");
	} catch (MyExceptionHander e) {
		// TODO Auto-generated catch block
		assertSame("Invalid username or password",e.getMessage());
	}	
}
}
