package testCases;

import daoClasses.DBController;
import junit.framework.TestCase;

public class DBControllerTest extends TestCase {

	// Test no.: 1
	// Test Objective: Test database connection
	// Input(s): none
	// Expected Outputs: connected to database successfully (boolean true)
	public void testDBControllerConnection() {
		// Create DB object
		DBController db = new DBController();
		// Test the method
		assertTrue(db.initiate_db_conn());	
	}

}
