package daoClasses;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import productCode.Order;
import productCode.Publication;



public class DBController {
	
	
	private Statement stmt = null;
	private ResultSet rs = null;
	private Connection con = null;

	public DBController() {
		super();
		initiate_db_conn();
		// TODO Auto-generated constructor stub
	}
	
	
	public boolean initiate_db_conn()
	{
		try
		{
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/newsAgency";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "admin");
			// Create a statement
			stmt = con.createStatement();
			return true;
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
			return false;
	}


	public boolean checklogin(String name, String pwd) {
		try
		{
			String updateTemp ="select * from employees "+"where userName= '"+name+"'"+"and pWord= '"+pwd+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Login success!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with login:\n"+sqle.toString());
			return false;
		}
	}

//Jessica test choose magazine and newspaper and granted code
	public boolean selectnewspaper(int id, String type) {
		try
		{
			String updateTemp ="select * from orders "+"where cust_id= '"+id+"'"+"and delivery_type= '"+type+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Select newspaper Success!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with select:\n"+sqle.toString());
			return false;
		}
	}
	
	public boolean selectmagazine(int id, String type) {
		try
		{
			String updateTemp ="select * from orders "+"where cust_id= '"+id+"'"+"and delivery_type= '"+type+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Select magazine Success!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with select:\n"+sqle.toString());
			return false;
		}
	}
	
	//Jessica test choose day of week code
	public boolean selectday(int id, String day) {
		try
		{
			String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Select Day Unsuccess!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with select:\n"+sqle.toString());
			return false;
		}
	}
	
	
	public boolean selectsunday(int id, String day) {
		try
		{
			String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Select Sunday Success!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with select:\n"+sqle.toString());
			return false;
		}
	}
	
	
	public boolean selectmonday(int id, String day) {
		try
		{
			String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Select Monday Success!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with select:\n"+sqle.toString());
			return false;
		}
	}
	
	
	public boolean selecttuesday(int id, String day) {
		try
		{
			String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Select Tuesday Success!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with select:\n"+sqle.toString());
			return false;
		}
	}
	
	
	public boolean selectwednesday(int id, String day) {
		try
		{
			String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Select Wednesday Success!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with select:\n"+sqle.toString());
			return false;
		}
	}
	
	
	public boolean selectthursday(int id, String day) {
		try
		{
			String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Select Thursday Success!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with select:\n"+sqle.toString());
			return false;
		}
	}
	
	
	public boolean selectfriday(int id, String day) {
		try
		{
			String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Select Friday Success!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with select:\n"+sqle.toString());
			return false;
		}
	}
	
	public boolean selectsaturday(int id, String day) {
		try
		{
			String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Select Saturday Success!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with select:\n"+sqle.toString());
			return false;
		}
	}

	
	public List<Publication> list(){
		List<Publication> PublicationList = new ArrayList<Publication>();
	     try {
	        String selectPublication="SELECT * FROM publications";
	        rs= stmt.executeQuery(selectPublication);
	        while (rs.next()) {
	        	Publication p = new Publication(rs.getInt("pub_id"), rs.getString("pubName"), rs.getInt("cost"));
	        	PublicationList.add(p);
	        }
	     } catch (SQLException sqle) {
	    	 sqle.printStackTrace();
	     }
	     return PublicationList;
	  }
}
