DROP DATABASE IF EXISTS newsAgency;
CREATE DATABASE IF NOT EXISTS newsAgency;
USE newsAgency;


DROP TABLE IF EXISTS employees;

CREATE TABLE employees (
	emp_id INTEGER AUTO_INCREMENT ,
	firstName VARCHAR(20) NOT NULL,
	lastName VARCHAR(20) NOT NULL,
	userName VARCHAR(20) NOT NULL,
	pWord VARCHAR(20) NOT NULL,
	contactNo VARCHAR(20) NOT NULL,
	PRIMARY KEY(emp_id) );

INSERT INTO employees VALUES ( null, 'Amy', 'Kenny', 'Amy1', 'A1234567', '08712345678');

SELECT * FROM employees;


DROP TABLE IF EXISTS customers;

CREATE TABLE customers (
	cust_id INTEGER AUTO_INCREMENT ,
	firstName VARCHAR(20) NOT NULL,
	lastName VARCHAR(20) NOT NULL,
	address VARCHAR(20) NOT NULL,
    contactNo VARCHAR(20) NOT NULL,
	loc_id INTEGER NOT NULL,
    emp_id INTEGER NOT NULL,
	PRIMARY KEY(cust_id), 
    FOREIGN KEY (loc_id) REFERENCES locations (loc_id),
    FOREIGN KEY (emp_id) REFERENCES employees (emp_id));

INSERT INTO customers VALUES ( null, 'John', 'Smith', '2 North State', '08787654321', 1, 1);

SELECT * FROM customers;


DROP TABLE IF EXISTS publications;

CREATE TABLE publications (

	pub_id INTEGER AUTO_INCREMENT ,
	pubName VARCHAR(20) NOT NULL,
    cost VARCHAR(20) NOT NULL,
    deliveryCost VARCHAR(20) NOT NULL,
	PRIMARY KEY(pub_id) );

INSERT INTO publications VALUES ( null, 'Athlone Advertiser', '1', '.50');


SELECT * FROM publications;


DROP TABLE IF EXISTS orders;

CREATE TABLE orders (

	ord_id INTEGER AUTO_INCREMENT ,
	quantity VARCHAR(20) NOT NULL,
	pub_id INTEGER NOT NULL,
	PRIMARY KEY(ord_id), 
    FOREIGN KEY (pub_id) REFERENCES publications (pub_id));

INSERT INTO orders VALUES ( null, 'Magazine', '100', 1);


SELECT * FROM orders;


DROP TABLE IF EXISTS daysOfWeek;

CREATE TABLE daysOfWeek (

	day_id INTEGER AUTO_INCREMENT ,
	dayOW VARCHAR(20) NOT NULL,
	PRIMARY KEY(day_id));

INSERT INTO daysOfWeek VALUES ( null, 'Sunday');
INSERT INTO daysOfWeek VALUES ( null, 'Monday');
INSERT INTO daysOfWeek VALUES ( null, 'Tuesday');
INSERT INTO daysOfWeek VALUES ( null, 'Wednesday');
INSERT INTO daysOfWeek VALUES ( null, 'Thursday');
INSERT INTO daysOfWeek VALUES ( null, 'Friday');
INSERT INTO daysOfWeek VALUES ( null, 'Saturday');


SELECT * FROM daysOfWeek;

DROP TABLE IF EXISTS locations;

CREATE TABLE locations (

	loc_id INTEGER AUTO_INCREMENT ,
	locName VARCHAR(20) NOT NULL,
    nor_id INTEGER NOT NULL,
    sou_id INTEGER NOT NULL,
    west_id INTEGER NOT NULL,
    east_id INTEGER NOT NULL,
	PRIMARY KEY(ord_id), 
    FOREIGN KEY (nor_id) REFERENCES northLocation (nor_id),
    FOREIGN KEY (sou_id) REFERENCES southLocation (sou_id),
    FOREIGN KEY (west_id) REFERENCES westLocation (west_id),
    FOREIGN KEY (east_id) REFERENCES eastLocation (east_id));

INSERT INTO locations VALUES ( null, 'Athlone Advertiser', 1);


SELECT * FROM locations;



DROP TABLE IF EXISTS northLocation;

CREATE TABLE northLocation (
	nor_id INTEGER AUTO_INCREMENT ,
    cust_id INTEGER NOT NULL,
	PRIMARY KEY(nor_id),
	FOREIGN KEY (cust_id) REFERENCES customers (cust_id));

INSERT INTO northLocation VALUES ( null, 1);

SELECT * FROM northLocation;


DROP TABLE IF EXISTS southLocation;

CREATE TABLE southLocation (
	sou_id INTEGER AUTO_INCREMENT ,
    cust_id INTEGER NOT NULL,
	PRIMARY KEY(sou_id),
	FOREIGN KEY (cust_id) REFERENCES customers (cust_id));

INSERT INTO southLocation VALUES ( null, 2);

SELECT * FROM southLocation;


DROP TABLE IF EXISTS eastLocation;

CREATE TABLE eastLocation (
	east_id INTEGER AUTO_INCREMENT ,
    cust_id INTEGER NOT NULL,
	PRIMARY KEY(east_id),
	FOREIGN KEY (cust_id) REFERENCES customers (cust_id));

INSERT INTO eastLocation VALUES ( null, 3);

SELECT * FROM eastLocation;


DROP TABLE IF EXISTS westLocation;

CREATE TABLE westLocation (
	west_id INTEGER AUTO_INCREMENT ,
    cust_id INTEGER NOT NULL,
	PRIMARY KEY(west_id),
	FOREIGN KEY (cust_id) REFERENCES customers (cust_id));

INSERT INTO westLocation VALUES ( null, 4);

SELECT * FROM westLocation;
